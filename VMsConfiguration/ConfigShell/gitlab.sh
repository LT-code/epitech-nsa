echo "gitlab.toto.com" >> /etc/hosts

sh add_interface/init_interfaces.sh
sh add_interface/add_static_interface.sh eth0 192.168.10.2 255.255.255.252 192.168.10.1

apt-get update
apt-get install -y curl openssh-server ca-certificates

# install postfix for gitlab
echo "postfix postfix/main_mailer_type select smarthost" | chroot / debconf-set-selections
echo "postfix postfix/mailname string gitlab.localdomain" | chroot / debconf-set-selections
echo "postfix postfix/relayhost string smtp.localdomain" | chroot / debconf-set-selections 

apt-get install -y postfix

# install gitlab
mkdir /etc/gitlab
echo "gitlab_rails['initial_root_password'] = \"rootroot\"" >> /etc/gitlab/gitlab.rb
echo "gitlab_rails['initial_shared_runners_registration_token'] = \"totoken\"" >> /etc/gitlab/gitlab.rb

# GITLAB_OMNIBUS_CONFIG="external_url 'http://localhost'; gitlab_rails['initial_root_password'] = \"rootroot\"; gitlab_rails['initial_shared_runners_registration_token'] = \"totoken\""
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
EXTERNAL_URL="http://gitlab.toto.com" GITLAB_ROOT_PASSWORD="rootroot"  apt-get -y install gitlab-ce 

#gitlab-ctl reconfigure

# install gitlab-runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
apt-get -y install gitlab-runner
#  
gitlab-runner start &

# install docker
apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io
systemctl enable docker

# connect gitlab-runner to gitlab
{
echo http://localhost
echo totoken
echo runner-front
echo runner-front
echo docker
echo node
} | gitlab-runner register --locked=false --run-untagged=true

{
echo http://localhost
echo totoken
echo runner-back
echo runner-back
echo docker
echo node
} | gitlab-runner register --locked=false --run-untagged=true


awk 'NR==9{print "  clone_url = \"http://192.168.10.2\""}1' /etc/gitlab-runner/config.toml > /etc/gitlab-runner/config.toml.new
mv /etc/gitlab-runner/config.toml.new /etc/gitlab-runner/config.toml
awk 'NR==30{print "  clone_url = \"http://192.168.10.2\""}1' /etc/gitlab-runner/config.toml > /etc/gitlab-runner/config.toml.new
mv /etc/gitlab-runner/config.toml.new /etc/gitlab-runner/config.toml

sed -i 's/url = "http:\/\/localhost"/url = "http:\/\/192.168.10.2"/g' /etc/gitlab-runner/config.toml

gitlab-runner stop runner-back
gitlab-runner start runner-back
gitlab-runner stop runner-front
gitlab-runner start runner-front
