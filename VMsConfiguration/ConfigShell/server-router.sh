network_number=$1

sh add_interface/init_interfaces.sh
sh add_interface/add_static_interface.sh eth0 192.168.$(echo $network_number)0.2 255.255.255.252 192.168.$(echo $network_number)0.1
sh add_interface/add_static_interface_no_gw.sh eth1 192.168.$(echo $network_number).1 255.255.255.248

sh internet_sharing/enable_sharing.sh eth0
sh internet_sharing/add_ip_foward.sh eth0 192.168.$network_number.2
sh internet_sharing/add_ip_foward.sh eth0 192.168.$network_number.3
sh internet_sharing/add_ip_foward.sh eth0 192.168.$network_number.4
#sh internet_sharing/add_ip_foward.sh eth1 192.168.20.1
sh internet_sharing/add_ip_foward.sh eth1 192.168.10.2

sh internet_sharing/port_redirection.sh eth0 80 80 192.168.$network_number.2
sh internet_sharing/port_redirection.sh eth0 8000 8000 192.168.$network_number.3

iptables-save > /etc/iptables/rules.v4
