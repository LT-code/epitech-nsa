echo "nameserver 8.8.8.8" > /etc/resolv.conf

sh add_interface/init_interfaces.sh
sh add_interface/add_dchp_interface.sh eth0
sh add_interface/add_static_interface_no_gw.sh eth1 192.168.10.1 255.255.255.252
sh add_interface/add_static_interface_no_gw.sh eth2 192.168.20.1 255.255.255.252
sh add_interface/add_static_interface_no_gw.sh eth3 192.168.30.1 255.255.255.252

sh add_interface/add_route.sh eth2 "192.168.2.0/29" 192.168.20.2

sh internet_sharing/enable_sharing.sh eth0
sh internet_sharing/add_ip_foward.sh eth0 192.168.10.2
sh internet_sharing/add_ip_foward.sh eth0 192.168.20.2

sh internet_sharing/port_redirection.sh eth0 80 80 192.168.10.2
sh internet_sharing/port_redirection.sh eth0 4200 80 192.168.20.2
sh internet_sharing/port_redirection.sh eth0 8000 8000 192.168.20.2

iptables -P FORWARD ACCEPT
iptables-save > /etc/iptables/rules.v4
