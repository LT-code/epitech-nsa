network_number=$1

gpasswd -a vagrant www-data
mkdir /var/www/html -p
chown  www-data:www-data /var/www/ -R
chmod 770 /var/www/html

sh add_interface/init_interfaces.sh
sh add_interface/add_static_interface.sh eth0 192.168.$network_number.3 255.255.255.248 192.168.$network_number.1
