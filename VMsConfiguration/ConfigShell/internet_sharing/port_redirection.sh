net_interface=$1
port=$2
portdest=$3
ipdest=$4

iptables -t nat -A PREROUTING -i $net_interface -p tcp --dport $port -j DNAT --to-destination $ipdest:$portdest
iptables -A FORWARD -i $net_interface -d $ipdest -p tcp --dport $port -j ACCEPT
