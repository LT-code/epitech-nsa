vmName=Router_1_Test
baseVmName=$1

sh utils/VMCreation.sh $baseVmName $vmName router1.sh 512

sh utils/bridgenet.sh $vmName 1 wlp8s0
sh utils/intnet.sh $vmName 2 Production
sh utils/intnet.sh $vmName 3 Staging
sh utils/intnet.sh $vmName 4 Gitlab