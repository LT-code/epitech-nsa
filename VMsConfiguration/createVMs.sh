#!/bin/sh

vagrant up

vboxmanage controlvm main-router poweroff soft
vboxmanage controlvm gitlab poweroff soft
vboxmanage controlvm staging-router poweroff soft
vboxmanage controlvm staging-back poweroff soft
vboxmanage controlvm staging-front poweroff soft
vboxmanage controlvm staging-mysql poweroff soft
vboxmanage controlvm production-router poweroff soft
vboxmanage controlvm production-back poweroff soft
vboxmanage controlvm production-front poweroff soft
vboxmanage controlvm production-mysql poweroff soft

sleep 6

# VBoxManage modifyvm router-1 --nic1 bridged --bridgeadapter1 wlp8s0
VBoxManage modifyvm main-router --nic2 intnet --intnet2 Gitlab
VBoxManage modifyvm main-router --nic3 intnet --intnet3 Staging
VBoxManage modifyvm main-router --nic4 intnet --intnet4 Production


VBoxManage modifyvm gitlab --nic1 intnet --intnet1 Gitlab

VBoxManage modifyvm staging-router --nic1 intnet --intnet1 Staging
VBoxManage modifyvm staging-router --nic2 intnet --intnet2 PrivateStaging
VBoxManage modifyvm staging-back --nic1 intnet --intnet1 PrivateStaging
VBoxManage modifyvm staging-front --nic1 intnet --intnet1 PrivateStaging
VBoxManage modifyvm staging-mysql --nic1 intnet --intnet1 PrivateStaging

VBoxManage modifyvm production-router --nic1 intnet --intnet1 Production
VBoxManage modifyvm production-router --nic2 intnet --intnet2 PrivateProduction
VBoxManage modifyvm production-back --nic1 intnet --intnet1 PrivateProduction
VBoxManage modifyvm production-front --nic1 intnet --intnet1 PrivateProduction
VBoxManage modifyvm production-mysql --nic1 intnet --intnet1 PrivateProduction

# start vms
vboxmanage startvm "main-router" --type headless
vboxmanage startvm "gitlab" --type headless
vboxmanage startvm "staging-router" --type headless
vboxmanage startvm "staging-front" --type headless
vboxmanage startvm "staging-back" --type headless
vboxmanage startvm "staging-mysql" --type headless

sleep 90

# git remote add origin https://github.com/username/new_repo


# creation des repos
cd ../NSA\ Code/nsa_back
cp ../.back-gitlab-ci.yml .gitlab-ci.yml 
yes | rm -r .git
git init
git add .
git config --global user.email "thomas.lopez@epitech.eu"
git config --global user.name "Thomas Lopez"
git commit -m "Initial commit"
git push --set-upstream http://root:rootroot@localhost:8080/root/nsa_back.git master

cd ../nsa_front
cp ../.front-gitlab-ci.yml .gitlab-ci.yml 
yes | rm -r .git
git init
git add .
git config --global user.email "thomas.lopez@epitech.eu"
git config --global user.name "Thomas Lopez"
git commit -m "Initial commit"
git push --set-upstream http://root:rootroot@localhost:8080/root/nsa_front.git master
