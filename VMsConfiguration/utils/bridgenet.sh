machineName=$1
interfaceNumber=$2
hostInterfaceName=$3

VBoxManage modifyvm $machineName --nic$interfaceNumber bridged --bridgeadapter$interfaceNumber $hostInterfaceName