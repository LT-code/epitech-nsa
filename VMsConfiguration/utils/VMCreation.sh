baseVmName=$1
machineName=$2
shellFile=$3
ram=$4

vboxmanage clonevm $baseVmName --name $machineName --snapshot base --mode machine --options link --register

VBoxManage modifyvm $machineName --cpus 1 --memory $ram --vram 12

echo "###################> VM starting"
VBoxManage startvm $machineName

echo "###################> Waiting an ssh connection"
machineIP=127.0.0.1
ssh-keyscan -p 2222 $machineIP >> ~/.ssh/known_hosts
while [ $? != 0 ]
do
	sleep 1
	ssh-keyscan -p 2222 127.0.0.1 >> ~/.ssh/known_hosts
done

export SSHPASS="toor"
./bin/sshpass.bin -e ssh -p 2222 user@$machineIP "ls"

echo "###################> Configuring vm by ssh"
./bin/sshpass.bin -e scp -P 2222 -r ConfigShell user@$machineIP:~/.
./bin/sshpass.bin -e ssh -p 2222 user@$machineIP << EOF 
cd ConfigShell
echo "toor" | sudo -S sh $shellFile
cd ..
rm -r ConfigShell
echo "toor" | sudo -S poweroff
EOF

echo "###################> Waiting machine stops"
sleep 4