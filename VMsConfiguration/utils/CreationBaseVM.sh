7z x baseDeb.7z
machineName=$1

echo "###################> VM Creation"
VBoxManage createvm --name $machineName --ostype Linux_64 --register
VBoxManage modifyvm $machineName --groups "/NSA Epitech"
VBoxManage modifyvm $machineName --cpus 1 --memory 1024 --vram 12

###############################
# base debian vm creation 
###############################

echo "###################> VM Network Config"
# create a network interface
#VBoxManage modifyvm $machineName --nic1 nat
VBoxManage modifyvm $machineName --natpf1 "guestssh,tcp,127.0.0.1,2222,,22"
#VBoxManage modifyvm $machineName --ànic1 bridged --bridgeadapter1 wlp8s0

# hdd attach
echo "###################> VM Hdd attach"
VBoxManage storagectl $machineName --name "SATA Controller" --add sata --bootable on
VBoxManage storageattach $machineName --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium ./baseDeb.vdi

vboxmanage snapshot $machineName take base