name=$1
host=$2

(
echo ~/.ssh/$name
echo 
echo 
) | ssh-keygen

ssh-copy-id -i ~/.ssh/$name root@$host -f

echo "IdentityFile ~/.ssh/$name" >> ~/.ssh/config
